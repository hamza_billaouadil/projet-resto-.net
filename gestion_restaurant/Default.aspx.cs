﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace gestion_restaurant
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        int idcommand;
        DataSet1TableAdapters.QueriesTableAdapter query;
        protected void Button1_Click(object sender, EventArgs e)
        {
            query =  new DataSet1TableAdapters.QueriesTableAdapter();
            query.ajouter_command("nouvelle",int.Parse(DropDownList3.SelectedValue));
            idcommand = int.Parse(query.select_command().ToString());
            query.ajouter_detailcomande(int.Parse(DropDownList1.SelectedValue), idcommand, int.Parse(DropDownList2.SelectedValue));
            Server.Transfer("Default.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            query = new DataSet1TableAdapters.QueriesTableAdapter();
            foreach (GridViewRow item in GridView1.Rows)
            {
                CheckBox c = (CheckBox)item.FindControl("check");
                if (c.Checked)
                {

                    query.modifie_etat_de_lacomande("paye", int.Parse(item.Cells[6].Text));

Server.Transfer("Default.aspx");
                }

            }
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
          
            query = new DataSet1TableAdapters.QueriesTableAdapter();
            foreach (GridViewRow item in GridView1.Rows)
            {
                CheckBox c = (CheckBox)item.FindControl("check");
                if (c.Checked)
                {

                    Session["idcommand"] = item.Cells[6].Text.ToString();  
                    Response.Redirect("modifie commande.aspx");


                }

            }


        }
    }
}
