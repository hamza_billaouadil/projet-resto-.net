﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="comptabilite.aspx.cs" Inherits="gestion_restaurant.comptabilite" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <p>
    <br />
</p>
<p>
    &nbsp; Nombres Des Commandes par table</p>
<p>
    &nbsp;
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        DataSourceID="SqlDataSource1" BackColor="White" BorderColor="#3366CC" 
        BorderStyle="None" BorderWidth="1px" CellPadding="4" Width="240px">
        <Columns>
            <asp:BoundField DataField="Expr1" HeaderText="nbre de commande" 
                ReadOnly="True" SortExpression="Expr1" />
            <asp:BoundField DataField="numero" HeaderText="numero de table" 
                SortExpression="numero" />
        </Columns>
        <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
        <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
        <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
        <RowStyle BackColor="White" ForeColor="#003399" />
        <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
        <SortedAscendingCellStyle BackColor="#EDF6F6" />
        <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
        <SortedDescendingCellStyle BackColor="#D6DFDF" />
        <SortedDescendingHeaderStyle BackColor="#002876" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_restaurantConnectionString7 %>" SelectCommand="SELECT     COUNT(detaille_commande.idcommande) AS Expr1, table1.numero
FROM         table1 INNER JOIN
                      serveur ON table1.idtable = serveur.idtable INNER JOIN
                      detaille_commande ON serveur.idserveur = detaille_commande.idserveur INNER JOIN
                      commande ON detaille_commande.idcommande = commande.idcommande
GROUP BY table1.numero"></asp:SqlDataSource>
    &nbsp;Gains par table</p>
<p>
    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
        BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" 
        CellPadding="4" DataSourceID="SqlDataSource2" Width="207px">
        <Columns>
            <asp:BoundField DataField="Column1" HeaderText="gains(DH)" 
                ReadOnly="True" SortExpression="Column1" />
            <asp:BoundField DataField="numero" HeaderText="numero de table" 
                SortExpression="numero" />
        </Columns>
        <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
        <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
        <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
        <RowStyle BackColor="White" ForeColor="#003399" />
        <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
        <SortedAscendingCellStyle BackColor="#EDF6F6" />
        <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
        <SortedDescendingCellStyle BackColor="#D6DFDF" />
        <SortedDescendingHeaderStyle BackColor="#002876" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_restaurantConnectionString8 %>" SelectCommand="SELECT ( SUM (element.prix )) , table1.numero
FROM         detaille_commande INNER JOIN
                      element ON detaille_commande.idelement = element.idelement INNER JOIN
                      serveur ON detaille_commande.idserveur = serveur.idserveur INNER JOIN
                      table1 ON serveur.idtable = table1.idtable INNER JOIN
                      commande ON detaille_commande.idcommande = commande.idcommande
group by     table1.numero"></asp:SqlDataSource>
</p>
<p>
    Nombre de Commande par serveur</p>
<p>
    <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" 
        BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" 
        CellPadding="4" DataSourceID="SqlDataSource3" Width="240px">
        <Columns>
            <asp:BoundField DataField="Column1" HeaderText="nbre de commande" 
                ReadOnly="True" SortExpression="Column1" />
            <asp:BoundField DataField="nom" HeaderText="nom serveur" 
                SortExpression="nom" />
        </Columns>
        <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
        <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
        <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
        <RowStyle BackColor="White" ForeColor="#003399" />
        <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
        <SortedAscendingCellStyle BackColor="#EDF6F6" />
        <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
        <SortedDescendingCellStyle BackColor="#D6DFDF" />
        <SortedDescendingHeaderStyle BackColor="#002876" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_restaurantConnectionString9 %>" SelectCommand="SELECT     count(commande.idcommande), serveur.nom
FROM         serveur INNER JOIN
                      detaille_commande ON serveur.idserveur = detaille_commande.idserveur INNER JOIN
                      commande ON detaille_commande.idcommande = commande.idcommande INNER JOIN
                      element ON detaille_commande.idelement = element.idelement
                      group by serveur.nom"></asp:SqlDataSource>
</p>
<p>
    gains par serveur</p>
<p>
    <asp:GridView ID="GridView4" runat="server" AutoGenerateColumns="False" 
        BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" 
        CellPadding="4" DataSourceID="SqlDataSource4" Width="224px">
        <Columns>
            <asp:BoundField DataField="nom" HeaderText="nom de serveur" SortExpression="nom" />
            <asp:BoundField DataField="Column1" HeaderText="gains(DH)" ReadOnly="True" 
                SortExpression="Column1" />
        </Columns>
        <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
        <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
        <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
        <RowStyle BackColor="White" ForeColor="#003399" />
        <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
        <SortedAscendingCellStyle BackColor="#EDF6F6" />
        <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
        <SortedDescendingCellStyle BackColor="#D6DFDF" />
        <SortedDescendingHeaderStyle BackColor="#002876" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_restaurantConnectionString10 %>" SelectCommand="SELECT     serveur.nom,sum( element.prix)
FROM         commande INNER JOIN
                      detaille_commande ON commande.idcommande = detaille_commande.idcommande INNER JOIN
                      element ON detaille_commande.idelement = element.idelement INNER JOIN
                      serveur ON detaille_commande.idserveur = serveur.idserveur
                      group by  serveur.nom"></asp:SqlDataSource>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
</asp:Content>
