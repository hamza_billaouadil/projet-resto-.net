﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace gestion_restaurant
{
    public partial class modifie_commande : System.Web.UI.Page
    {
        DataSet1TableAdapters.QueriesTableAdapter query;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["idcommand"].ToString()== "")
            { Response.Redirect("default.aspx"); }

            Label7.Text = Session["idcommand"].ToString();
            using (SqlConnection cn = new SqlConnection("data source=.;integrated security=true;initial catalog=db_restaurant"))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand(@"SELECT     table1.numero, serveur.nom FROM         detaille_commande INNER JOIN
                      serveur ON detaille_commande.idserveur = serveur.idserveur INNER JOIN
                      table1 ON serveur.idtable = table1.idtable
where detaille_commande.idcommande = " + int.Parse(Label7.Text) + "", cn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Label5.Text = dr[1].ToString();
                    Label6.Text = dr[0].ToString();
                }

            }


        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            query = new DataSet1TableAdapters.QueriesTableAdapter();
            query.modifie_element(int.Parse(DropDownList1.SelectedValue), int.Parse(Label7.Text));
            query.modifie_quantite(int.Parse(DropDownList2.SelectedValue), int.Parse(Label7.Text));
            Response.Redirect("Default.aspx");
        }
    }
}