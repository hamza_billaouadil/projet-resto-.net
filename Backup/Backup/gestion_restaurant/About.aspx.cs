﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace gestion_restaurant
{
    public partial class About : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        DataSet1TableAdapters.QueriesTableAdapter query;

        protected void Button1_Click(object sender, EventArgs e)
        {
            query = new DataSet1TableAdapters.QueriesTableAdapter();
            foreach (GridViewRow item in GridView1.Rows)
            {
                CheckBox c = (CheckBox)item.FindControl("check");
                if (c.Checked)
                {
                    
                query.modifie_etat_de_lacomande("en cours de préparation",int.Parse(item.Cells[6].Text));
                Server.Transfer("About.aspx");
                
                
                }
            
            }

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            query = new DataSet1TableAdapters.QueriesTableAdapter();
            foreach (GridViewRow item in GridView1.Rows)
            {
                CheckBox c = (CheckBox)item.FindControl("check");
                if (c.Checked)
                {

                    query.modifie_etat_de_lacomande("servie", int.Parse(item.Cells[6].Text));
                    Server.Transfer("About.aspx");


                }

            }
        }
    }
}
