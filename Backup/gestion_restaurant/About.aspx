﻿<%@ Page Title="Qui sommes-nous" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="About.aspx.cs" Inherits="gestion_restaurant.About" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style3
        {
            width: 197px;
        }
        .style4
        {
            width: 206px;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <p>
        <br />
    </p>
    <p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label1" runat="server" Text="Lies des Commandes"></asp:Label>
&nbsp;&nbsp;</p>
    <p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataSourceID="SqlDataSource1" Width="651px" BackColor="White" 
            BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="4" 
            DataKeyNames="idcommande">
            <Columns>
                <asp:BoundField DataField="numero" HeaderText="numero" 
                    SortExpression="numero" />
                <asp:BoundField DataField="etat" HeaderText="etat" SortExpression="etat" />
                <asp:BoundField DataField="nom" HeaderText="plat" SortExpression="nom" />
                <asp:BoundField DataField="quantite" HeaderText="quantite" 
                    SortExpression="quantite" />
                <asp:BoundField DataField="prix" HeaderText="prix" SortExpression="prix" />
                <asp:BoundField DataField="Expr1" HeaderText="serveur" SortExpression="Expr1" />
                <asp:BoundField DataField="idcommande" HeaderText="idcommande" 
                    InsertVisible="False" ReadOnly="True" SortExpression="idcommande" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:CheckBox ID="check" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
            <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
            <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
            <RowStyle BackColor="White" ForeColor="#003399" />
            <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
            <SortedAscendingCellStyle BackColor="#EDF6F6" />
            <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
            <SortedDescendingCellStyle BackColor="#D6DFDF" />
            <SortedDescendingHeaderStyle BackColor="#002876" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:db_restaurantConnectionString %>" 
            
            
            
            SelectCommand="SELECT table1.numero, commande.etat, element.prix, element.nom, serveur.nom AS Expr1, commande.idcommande, commande.quantite FROM table1 INNER JOIN serveur ON table1.idtable = serveur.idtable INNER JOIN detaille_commande ON serveur.idserveur = detaille_commande.idserveur INNER JOIN commande ON detaille_commande.idcommande = commande.idcommande INNER JOIN element ON detaille_commande.idelement = element.idelement WHERE (commande.etat &lt;&gt; 'servie') AND (commande.etat &lt;&gt; 'servie')">
        </asp:SqlDataSource>
    </p>
    <p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <table class="style1">
            <tr>
                <td class="style4">
                    &nbsp;<asp:Button ID="Button1" runat="server" Height="48px" onclick="Button1_Click" 
                        Text="en cours de preparation" Width="151px" />
                    &nbsp;</td>
                <td class="style3">
                    <asp:Button ID="Button2" runat="server" Height="51px" onclick="Button2_Click" 
                        style="margin-top: 0px" Text="Servie" Width="179px" />
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
        &nbsp;</p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
</asp:Content>
